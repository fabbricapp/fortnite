package com.fabbricapp.fortnite;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TimePicker;

import com.fabbricapp.fortnite.broadcastReceivers.AlarmReciever;
import com.fabbricapp.fortnite.utils.CommonWidget;

import java.util.Calendar;
import java.util.Date;

public class Reminder extends AppCompatActivity implements View.OnClickListener {
private TimePicker timep;
private Button applyb;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reminder);
        timep = (TimePicker)findViewById(R.id.timePicker);
        applyb = (Button)findViewById(R.id.button2);
        applyb.setOnClickListener(this);
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onClick(View view) {
        if(view.getId()==R.id.button2){
            Log.e("time",timep.getHour()+" "+timep.getMinute());
            CommonWidget.RingAlarm(this,timep.getHour(),timep.getMinute());
        }
    }
}
