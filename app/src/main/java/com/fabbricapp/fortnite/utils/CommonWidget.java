package com.fabbricapp.fortnite.utils;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.NotificationCompat;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.widget.Toast;

import com.fabbricapp.fortnite.MainActivity;
import com.fabbricapp.fortnite.R;
import com.fabbricapp.fortnite.broadcastReceivers.AlarmReciever;
import com.fabbricapp.fortnite.MainActivity;
import com.fabbricapp.fortnite.model.User;
import com.google.gson.Gson;

import java.util.Calendar;
import java.util.Date;
import java.util.Random;

/**
 * Created by marwensarraj on 04/07/2018.
 */

public class CommonWidget {
    public static User model;
    public static final String key_login = "login";
    public static SharedPreferences sharedpreferences;
    public static SharedPreferences.Editor editor;

    //TODO: change
    public static final String prefString = "fortnite";

    public static void showMsg(Context context, String message) {

        try {
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    public static void showOKDialog(final Context context, String title,
                                    String message, DialogInterface.OnClickListener btnPositiveClickListener) {

        try {
            AlertDialog.Builder builder1 = new AlertDialog.Builder(context);

            builder1.setTitle("");
            builder1.setMessage(message);
            builder1.setCancelable(false);
            builder1.setNeutralButton(android.R.string.ok,
                    btnPositiveClickListener);

            AlertDialog alert11 = builder1.create();
            alert11.setCanceledOnTouchOutside(false);
            alert11.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    public static void showAlertDialog(final Context context, String title,
                                       String message, final boolean isfinish) {

        try {
            AlertDialog.Builder builder1 = new AlertDialog.Builder(context);

            builder1.setTitle("");
            builder1.setMessage(message);
            builder1.setCancelable(false);
            builder1.setNeutralButton(android.R.string.ok,
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            if (isfinish) {
                                ((Activity) context).finish();
                            }
                        }
                    });

            AlertDialog alert11 = builder1.create();
            alert11.setCanceledOnTouchOutside(false);
            alert11.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    public static void logutFromApplication(Activity activity, User userLoginModel) {
        /*MessageSource.getInstance(activity, userLoginModel).stop();
        FacebookHelper.getInstance(activity).logout();
        DatabaseHandler.getInstance(activity, userLoginModel).logout();
        AppSetting.clearAllPreference(activity.getApplicationContext());
        Chat.resetChatData();*/
    }

    public static User getUserData(Context context) {

        if (model == null) {
            String jsonResult = getLoginData(context);
            if (TextUtils.isEmpty(jsonResult)) return null;

            try {
                Gson gson = new Gson();
                model = gson.fromJson(jsonResult, User.class);

            } catch (Exception e) {
                e.printStackTrace();
                model = null;
                return model;
            }
        }

        return model;
    }



    public static void setLoginData(Context context, String val) {
        sharedpreferences = context.getSharedPreferences(prefString, Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();
        editor.putString(key_login, val);
        editor.commit();

    }

    public static String getLoginData(Context context) {

        sharedpreferences = context.getSharedPreferences(prefString, Context.MODE_PRIVATE);
        return sharedpreferences.getString(key_login, "");

    }

    public static String getApi() {

        String[] listApi = {"fc289300-369f-473e-9bba-b0d455d4833f","10abb772-1caf-490b-a075-76816e970aa0","1bd534f0-b6a3-41d6-9ce6-c0e20aa808b6","72ee1e10-4471-48ba-9e22-07ebb08a2488"};
        Random r = new Random();
        int i = r.nextInt(4);
        //return listApi[i];
        return "1352a8bd-9e75-4087-957f-1edf76ad1456";
    }

    public static void RingAlarm (Context context,int h,int m){
        Date dat  = new Date();//initializes to now
        Calendar cal_alarm = Calendar.getInstance();
        Calendar cal_now = Calendar.getInstance();
        cal_now.setTime(dat);
        cal_alarm.setTime(dat);
        cal_alarm.set(Calendar.HOUR_OF_DAY,h);//set the alarm time
        cal_alarm.set(Calendar.MINUTE, m);
        cal_alarm.set(Calendar.SECOND,0);
        if(cal_alarm.before(cal_now)){//if its in the past increment
            cal_alarm.add(Calendar.DATE,1);
        }

        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, AlarmReciever.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context,0010000,intent,0);

        alarmManager.set(AlarmManager.RTC_WAKEUP,cal_alarm.getTimeInMillis(),pendingIntent);
    }
    public static void drawNotification(Context context, String text){
        NotificationManager notificationManager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
        Intent mIntent = new Intent(context, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, mIntent.getIntExtra("notifId", 0), mIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
        builder.setContentTitle("App");
        builder.setContentText("Notify Me");
        builder.setAutoCancel(true);
        builder.setSmallIcon(R.mipmap.ic_launcher);
        builder.setContentIntent(pendingIntent);

        notificationManager.notify(mIntent.getIntExtra("notifId", 0), builder.build());

        //return super.onStartCommand(mIntent, flag, startId);
    }

    public synchronized static boolean isConnectingToInternet(Context _context) {
        ConnectivityManager connectivity = (ConnectivityManager) _context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo nInfo = connectivity.getActiveNetworkInfo();

            if (nInfo != null && nInfo.getState() == NetworkInfo.State.CONNECTED) {
                //do your thing
                return true;
            }
            /*NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++) {
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
                }
*/
        }
        return false;
    }

}
