package com.fabbricapp.fortnite.utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;

import com.fabbricapp.fortnite.R;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * Created by marwensarraj on 05/07/2018.
 */

public class Utility {
    public static final String TAG = "Utility";
    private static Utility instance;
    private int[] randomColors;
    private Context context;
    public static ProgressDialog progressDialog;

    public Utility(Context context) {
        this.context = context;
        randomColors = this.context.getResources().getIntArray(R.array.randomcolors);
    }

    public static Utility getInstance(Context context) {
        if (instance == null) {
            instance = new Utility(context);
        }
        return instance;
    }

    public int getRandomcolor() {
        return randomColors[new Random().nextInt(randomColors.length)];
    }

    public static String getUTF8String(byte[] targetByte) {
        try {
            return new String(targetByte, "UTF-8");
        } catch (Exception e) {
            return "";
        }

    }

    public static String getStringFromJSON(String jsonResult, String key) {
        try {
            return new JSONObject(jsonResult).getString(key);
        } catch (Exception e) {
            return "";
        }
    }
    public static String getIPAddress(boolean useIPv4) {
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
                for (InetAddress addr : addrs) {
                    if (!addr.isLoopbackAddress()) {
                        String sAddr = addr.getHostAddress();
                        //boolean isIPv4 = InetAddressUtils.isIPv4Address(sAddr);
                        boolean isIPv4 = sAddr.indexOf(':') < 0;

                        if (useIPv4) {
                            if (isIPv4)
                                return sAddr;
                        } else {
                            if (!isIPv4) {
                                int delim = sAddr.indexOf('%'); // drop ip6 zone suffix
                                return delim < 0 ? sAddr.toUpperCase() : sAddr.substring(0, delim).toUpperCase();
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) {
        } // for now eat exceptions
        return "";
    }
    public static void showProgressDialog(Activity mActivity,
                                          DialogInterface.OnCancelListener cancelListener) {
// TODO Auto-generated method stub
        if (mActivity != null) {
            progressDialog = ProgressDialog.show(mActivity, null, mActivity.getResources().getString(R.string.msg_progress_dialogue), true,
                    true, cancelListener);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setOnCancelListener(cancelListener);
        }
        Log.v("ProgressDialog", "show ");
    }

    public static void dismissProgressDialog() {
        Log.v("ProgressDialog", "dismiss");
        if (progressDialog != null)
            progressDialog.dismiss();
    }

    public static void showProgressDialog(Activity mActivity,
                                          DialogInterface.OnCancelListener cancelListener, String msg) {
// TODO Auto-generated method stub
        progressDialog = ProgressDialog.show(mActivity, null, msg, true,
                true, cancelListener);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setOnCancelListener(cancelListener);
        Log.v("ProgressDialog", "show ");
    }
}
