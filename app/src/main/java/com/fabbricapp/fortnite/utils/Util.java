package com.fabbricapp.fortnite.utils;

import android.os.Bundle;

import com.fabbricapp.fortnite.MainApplication;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.fabbricapp.fortnite.MainApplication;

/**
 * Created by marwensarraj on 04/07/2018.
 */

public class Util {
    public static void analyticLog(String eventName,Bundle bundle){
        MainApplication.mFirebaseAnalytics.logEvent(eventName,bundle);
    }
}
