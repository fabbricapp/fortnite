package com.fabbricapp.fortnite;

import android.content.ContextWrapper;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.fabbricapp.fortnite.webServices.WebserviceURLs;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.loopj.android.http.AsyncHttpClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {
    private String TAG="LoginActivity";
    LoginButton fbloginButton;
    CallbackManager callbackManager;
    AsyncHttpClient async;
    private static RequestQueue queue;
    private static JSONObject responseObject;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        fbloginButton=(LoginButton)findViewById(R.id.login_button);
        callbackManager = CallbackManager.Factory.create();
        fbloginButton.setReadPermissions("public_profile");
        fbloginButton.setReadPermissions("email");
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        String accessToken = loginResult.getAccessToken().getToken();
                        // save accessToken to SharedPreference
                        SharedPreferences prefs = getSharedPreferences("FacebookProfile", ContextWrapper.MODE_PRIVATE);
                        SharedPreferences.Editor editor = prefs.edit();
                        editor.putString("fb_access_token", accessToken);
                        editor.apply();
                        GraphRequest request = GraphRequest.newMeRequest(
                                loginResult.getAccessToken(),
                                (jsonObject, response) -> {
                                    getFacebookData(jsonObject);
                                });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id,first_name,last_name,email,gender");
                        request.setParameters(parameters);
                        request.executeAsync();
                    }

                    @Override
                    public void onCancel() {
                        // App code
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code
                    }
                });

    }

    private void getFacebookData(JSONObject object) {

        try {
            String id = object.getString("id");
            MainApplication.store("facebookId", id);
            MainApplication.loggedUser.facebookId=id;
            Log.d("dssgsdg : ",id);
            URL profile_pic;
            try {
                profile_pic = new URL("https://graph.facebook.com/" + id + "/picture?type=large");
                {MainApplication.store("profileImg", profile_pic.toString());
                MainApplication.loggedUser.profileImg=profile_pic.toString();}
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            if (object.has("first_name")){
                MainApplication.store("firstName", object.getString("first_name"));
                MainApplication.loggedUser.firstName=object.getString("first_name");}
            if (object.has("last_name")) {
                MainApplication.store("lastName", object.getString("last_name"));
                MainApplication.loggedUser.lastName = object.getString("last_name");
            }
            MainApplication.commit();
            synchdata();
        } catch (Exception e) {
            Log.d(TAG, "BUNDLE Exception : "+e.toString());
        }

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void synchdata() throws AuthFailureError {
        final String url = WebserviceURLs.USER_URL+"/loginx";
        queue = Volley.newRequestQueue(this);
        Map<String, String> params = new HashMap<String, String>();
        params.put("facebookId", "rm@test.com.br");

        // prepare the Request
        JSONObject parameters = new JSONObject(params);
        Log.e("**",parameters.toString());

        Log.e("$$",url);
        JsonObjectRequest jsonObjRequest = new JsonObjectRequest(Request.Method.POST,url,null ,
                new Response.Listener<JSONObject>()
            {
                @Override
                public void onResponse(JSONObject responseObject) {
                    // display response
                    try {
                        Log.e("Responsedata",responseObject.toString());

                        Log.e("Responsedata",responseObject.toString());
                        if(responseObject.getString("status")=="1"){
                            MainApplication.loggedUser.setData(responseObject.getJSONObject("data"));
                            startprofil();
                        }
                        if(responseObject.getString("status")=="2"){
                            MainApplication.loggedUser.setData(responseObject.getJSONObject("data"));
                            startmenu();
                        }
                    } catch (JSONException e) {
                        Log.e("Responsedata",e.getMessage());
                    } catch (Exception e) {
                        Log.e("Responsedata", e.getMessage());
                    }
                }
            },
            new Response.ErrorListener()
            {
                @Override
                public void onErrorResponse(VolleyError error) {

                    Log.e("Error.Response", error.toString());
                }
            }
         ){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                return headers;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("facebookId", "rm@test.com.br");


                return params;
            }
        };
        Log.e("",jsonObjRequest.getBodyContentType());
        Log.e("",jsonObjRequest.getUrl());


        // add it to the RequestQueue
        queue.add(jsonObjRequest);
    }
    public void startprofil(){
        Intent intent=new Intent(this,BuildProfileActivity.class);
        startActivity(intent);
    }
    public void startmenu(){
        Intent intent=new Intent(this,MenuActivity.class);
        startActivity(intent);
    }

}
