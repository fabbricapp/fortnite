package com.fabbricapp.fortnite.notificationFCM;

import android.util.Log;

import com.fabbricapp.fortnite.MainApplication;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by marwensarraj on 04/07/2018.
 */

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {
    private String TAG="MyFirebaseInstanceIDService";
    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.e(TAG, "Refreshed token: " + refreshedToken);
        MainApplication.store("deviceToken",refreshedToken);
        MainApplication.commit();
        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        sendRegistrationToServer(refreshedToken);
    }

    private void sendRegistrationToServer(String refreshedToken) {

    }
}
