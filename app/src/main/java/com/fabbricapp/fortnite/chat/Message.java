package com.fabbricapp.fortnite.chat;

/**
 * Created by marwensarraj on 13/07/2018.
 */

import android.graphics.Bitmap;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.firebase.client.ServerValue;

import java.io.File;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Message
{


    @JsonIgnore
    private String TAG = "Message";
    @JsonIgnore
    private String messageID="";

    @JsonIgnore
    public static final int TEXT_TYPE =0;
    @JsonIgnore
    public static final int IMAGE_TYPE =1;

    // if messageType is o then it will be text message
    // if messageType is 1 then it will be image message
    // if messageType is 2 then it will be video message . not implemented yet
    public int messageType=TEXT_TYPE;

    private long mSenderUserId;
    private long mReceiverId;
    private String senderName="";
    private String receiverName="";
    private String senderAvtarUrl ="";
    private String receiverAvtarURL="";
    private String message="";
    private long timeStamp;

    @JsonIgnore
    private boolean isSent =true;
    private String imageThumbnail="";
    @JsonIgnore
    private String attachmentLocalImagePath="";
    private String attachmentServerImagePath="";
    @JsonIgnore
    private Bitmap bitmap;
    @JsonIgnore
    public boolean isUploading =false; // this variable will be true if
    @JsonIgnore
    public boolean isDownloading =false;

    @JsonIgnore
    public File imageFile;


    private String attachmentSize ="";

    public String getAttachmentSize() {
        return attachmentSize;
    }

    public void setAttachmentSize(String attachmentSize) {
        this.attachmentSize = attachmentSize;
    }

    public String getImageThumbnail() {
        return imageThumbnail;
    }

    public void setImageThumbnail(String imageThumbnail) {
        this.imageThumbnail = imageThumbnail;
    }

    public String getAttachmentServerImagePath()
    {
        return attachmentServerImagePath;
    }

    public void setAttachmentServerImagePath(String attachmentServerImagePath)
    {
        this.attachmentServerImagePath = attachmentServerImagePath;
    }

    @JsonIgnore
    public String getAttachmentLocalImagePath()
    {
        return attachmentLocalImagePath;
    }

    @JsonIgnore
    public void setAttachmentLocalImagePath(String attachmentLocalImagePath) {
        this.attachmentLocalImagePath = attachmentLocalImagePath;
    }



    public String getMessageID() {
        return messageID;
    }

    public void setMessageID(String messageID) {
        this.messageID = messageID;
    }

    public int getMessageType() {
        return messageType;
    }

    public void setMessageType(int messageType) {
        this.messageType = messageType;
    }

    public boolean isSent() {
        return isSent;
    }

    public void setIsSent(boolean isSent) {
        this.isSent = isSent;
    }

    public String getReceiverAvtarURL()
    {
        return receiverAvtarURL;
    }

    public void setReceiverAvtarURL(String receiverAvtarURL) {
        this.receiverAvtarURL = receiverAvtarURL;
    }

    public String getReceiverName() {

        return receiverName;
    }

    public void setReceiverName(String receiverName) {
        this.receiverName = receiverName;
    }

    public String getSenderAvtarUrl() {
        return senderAvtarUrl;
    }

    public void setSenderAvtarUrl(String senderAvtarUrl) {
        this.senderAvtarUrl = senderAvtarUrl;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public Message() {

    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public long getmReceiverId() {
        return mReceiverId;
    }

    public void setmReceiverId(long mReceiverId) {
        this.mReceiverId = mReceiverId;
    }

    public long getmSenderUserId()
    {
        return mSenderUserId;
    }

    public void setmSenderUserId(long mSenderUserId) {
        this.mSenderUserId = mSenderUserId;
    }

    public void setTimeStamp(long timeStamp)
    {
        this.timeStamp = timeStamp;
    }

    @JsonIgnore
    public long getTimeStampLong()
    {
        //Log.d(TAG, "getTimeStamp() returned: " + ServerValue.TIMESTAMP);
        return this.timeStamp;
    }

    public Map<String,String> getTimeStamp()
    {
        return ServerValue.TIMESTAMP;
    }

    @Override
    public String toString()
    {
        return "{ Key id                 =>"+ getMessageID() +
                "Sender id             =>"+ getmSenderUserId() +
                "\nReceiverId          =>"+getmReceiverId() +
                "\nSender name         =>"+getSenderName()+
                "\nReceiver Name       =>"+getReceiverName()+
                "\nSenderAvtarURL      =>"+getSenderAvtarUrl()+
                "\nReceiverAvtarURl    =>"+getReceiverAvtarURL()+
                "\nMessage             =>"+getMessage()+
                "\nTimestamp           =>"+getTimeStampLong()+

                "}";

    }

    public void updateMessage(Message message)
    {
        message.setTimeStamp(message.getTimeStampLong());
    }


    public void setBitmap(Bitmap bitmap)
    {
        this.bitmap = bitmap;
    }

    public Bitmap getBitmap()
    {
        return bitmap;
    }

}
