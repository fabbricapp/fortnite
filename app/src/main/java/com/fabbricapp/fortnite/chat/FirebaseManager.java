package com.fabbricapp.fortnite.chat;
import com.firebase.client.Firebase;

/**
 * Created by marwensarraj on 13/07/2018.
 */

public class FirebaseManager {

    //TODO: PROD/TEST
    private static String FIREBASE_URL = "https://black-desert-8a210.firebaseio.com/users";

    private static String FIREBASE_SEARCH_URL = "https://black-desert-8a210.firebaseio.com/service";
    private static String FIREBASE_USERPROFILE_URL ="https://black-desert-8a210.firebaseio.com/userprofile";
    /*TEST*/
    //private static String FIREBASE_URL = "https://jugglr-test.firebaseio.com/users";
    //private static String FIREBASE_SEARCH_URL = (JugglrApplication.country.equalsIgnoreCase("us"))?"https://jugglr-test.firebaseio.com/service-us":"https://jugglr-test.firebaseio.com/service";
    //private static String FIREBASE_USERPROFILE_URL = (JugglrApplication.country.equalsIgnoreCase("us"))?"https://jugglr-test.firebaseio.com/userprofile-us":"https://jugglr-test.firebaseio.com/userprofile";


    private Firebase mFirebaseRefAtSender = new Firebase(FIREBASE_URL);
    private Firebase mFirebaseRefAtSearch = new Firebase(FIREBASE_SEARCH_URL);
    private Firebase mFirebaseRefUserProfile = new Firebase(FIREBASE_USERPROFILE_URL);

    private static FirebaseManager firebaseManager;
    //TODO: PROD/TEST
    public FirebaseManager() {

    }

    public static FirebaseManager getInstance() {
        if (firebaseManager == null )
            firebaseManager = new FirebaseManager();
        return firebaseManager;
    }

    public Firebase getUserIdFireBase(String st) {
        return mFirebaseRefAtSender.child(st);
    }

    public Firebase getMessageFireBase(String id) {
        return mFirebaseRefAtSender.child(id).child("message");
    }

    public Firebase getUserPreferenceFirebase(String id) {
        return mFirebaseRefAtSender.child(id).child("Userpreferences");
    }

    public Firebase getProfileFirebase(String id) {
        return mFirebaseRefAtSender.child(id).child("profile");
    }


    public Firebase getUserProfileData(String id) {
        return mFirebaseRefUserProfile.child(id);
    }


    public Firebase updateUserProfile(String userID) {
        return mFirebaseRefUserProfile.child(userID).child("profile-image");
    }

    public Firebase getBase() {
        return mFirebaseRefAtSender;
    }


    public void updateProfile(int UserId) {
        Firebase profileFirebase = getProfileFirebase(String.valueOf(UserId));

    }


    public Firebase getNeedSubServices(String id) {
        return mFirebaseRefAtSearch.child("need").child("subservice").child(id);
    }

    public Firebase getNeedSubServicesTAG(String id) {
        return mFirebaseRefAtSearch.child("need").child("subservicetag").child(id);
    }

    public Firebase getOfferSubServices(String id) {
        return mFirebaseRefAtSearch.child("offer").child("subservice").child(id);
    }

    public Firebase getOfferSubServicesTAG(String id) {
        return mFirebaseRefAtSearch.child("offer").child("subservicetag").child(id);
    }
}
