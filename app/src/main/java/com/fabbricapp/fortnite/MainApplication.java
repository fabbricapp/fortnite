package com.fabbricapp.fortnite;

import android.app.Application;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.fabbricapp.fortnite.model.User;
import com.fabbricapp.fortnite.utils.DBHelper;
import com.fabbricapp.fortnite.utils.FontsOverride;
import com.fabbricapp.fortnite.utils.TypefaceUtil;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.crash.FirebaseCrash;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Locale;

import io.fabric.sdk.android.Fabric;

/**
 * Created by marwensarraj on 04/07/2018.
 */

public class MainApplication extends Application {
    public static FirebaseAnalytics mFirebaseAnalytics;
    public static FirebaseDatabase firebaseDatabase;
    public static SharedPreferences prefs;
    public static SharedPreferences.Editor editor;
    public static User loggedUser;
    public static String refreshedToken;
    @Override
    public void onCreate() {
        super.onCreate();
        prefs = getSharedPreferences(getPackageName(), ContextWrapper.MODE_PRIVATE);
        editor = prefs.edit();
        Fabric.with(this, new Crashlytics());
        try{
            DBHelper.getinstance(this);
        }catch (IOException e){
            Log.d("DBHELPER", e.getMessage());
        }
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        firebaseDatabase = FirebaseDatabase.getInstance();
        FirebaseCrash.setCrashCollectionEnabled(true);
        refreshedToken = FirebaseInstanceId.getInstance().getToken();
        store("deviceToken",refreshedToken);
        commit();
        inituser();
        FontsOverride.setDefaultFont(this, "MONOSPACE", "luckiest.ttf");

        //TypefaceUtil.overrideFont(getApplicationContext(), "SERIF", "fonts/luckiest.ttf");
        //Log.e(null,null);


    }
    public static void store(String key, String val){
        editor.putString(key, val);
    }
    public static String retrive(String key, String defaultVal){
        return prefs.getString(key, defaultVal);

    }
    public static void commit(){
        editor.apply();
    }
    public void inituser(){
        loggedUser=new User();
        loggedUser.facebookId=MainApplication.retrive("facebookId", null);
        loggedUser.profileImg=MainApplication.retrive("profileImg", null);
        loggedUser.firstName=MainApplication.retrive("firstName", null);
        loggedUser.lastName=MainApplication.retrive("lastName", null);
        loggedUser.ingameName=MainApplication.retrive("ingameName", null);
        loggedUser.avatar=MainApplication.retrive("avatar", null);
        loggedUser.serverName=MainApplication.retrive("serverName", null);
        loggedUser.platformName=MainApplication.retrive("platformName", null);
        loggedUser.party=MainApplication.retrive("party", null);
        loggedUser.stat=MainApplication.retrive("stat", null);
        loggedUser.deviceToken=refreshedToken;
    }
}
