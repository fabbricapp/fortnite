package com.fabbricapp.fortnite;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.fabbricapp.fortnite.adapter.ImageAdapter;
import com.fabbricapp.fortnite.utils.CommonWidget;
import com.fabbricapp.fortnite.webServices.WebserviceURLs;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

public class BuildProfileActivity extends AppCompatActivity {
    int positionImage = -1;
    private String TAG="BuildProfileActivity";
    ProgressDialog dialog;
    RequestQueue queue;
    FirebaseDatabase database;
    DatabaseReference myRef;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_build_profile);
        database = FirebaseDatabase.getInstance();
        myRef = database.getReference("users");

        //GridView
        GridView gridview = (GridView) findViewById(R.id.gvAvatar);
        gridview.setAdapter(new ImageAdapter(this));
        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {
                positionImage=position;
            }
        });
        //Name Input
        EditText etInGameName = (EditText) findViewById(R.id.etInGameName);
        //Apply Button
        Button btApply = (Button) findViewById(R.id.btApply);
        //Server List
        Spinner spServer = (Spinner) findViewById(R.id.spServer);
        ArrayAdapter<CharSequence> adapterServer = ArrayAdapter.createFromResource(this,
                R.array.servers_array, android.R.layout.simple_spinner_item);
        adapterServer.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spServer.setAdapter(adapterServer);

        //Platform List
        Spinner spPlatform = (Spinner) findViewById(R.id.spPlatform);
        ArrayAdapter<CharSequence> adapterPlatform = ArrayAdapter.createFromResource(this,
                R.array.platform_array, android.R.layout.simple_spinner_item);
        adapterPlatform.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spPlatform.setAdapter(adapterPlatform);
        //Switch
        Switch swDiscover = (Switch) findViewById(R.id.switch1);


        btApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (positionImage>-1
                        &&etInGameName!=null
                        &&!spServer.getSelectedItem().toString().equals("Select your server")
                        &&!spPlatform.getSelectedItem().toString().equals("Select your platform")){

                    //Todo: Save data
                    MainApplication.loggedUser.ingameName=etInGameName.getText().toString();
                    MainApplication.loggedUser.serverName=spServer.getSelectedItem().toString();
                    MainApplication.loggedUser.platformName=spPlatform.getSelectedItem().toString();
                    MainApplication.loggedUser.party=swDiscover.isChecked()?"1":"0";
                    synch();

                }else {
                    CommonWidget.showMsg(getApplicationContext(),"Verify data");
                }
            }
        });
    }

    public void synch(){
        final String url = WebserviceURLs.USER_URL+"/refresh";
        queue = Volley.newRequestQueue(BuildProfileActivity.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    //Creating JsonObject from response String
                    Log.e(TAG,response);
                    JSONObject responseObject= new JSONObject(response.toString());
                    Log.e(TAG,responseObject.getString("status")+"");
                    dialog.dismiss();

                    if(responseObject.getString("status").equals("1")){
                        MainApplication.loggedUser.setData(responseObject.getJSONObject("data"));
                        myRef.child(""+MainApplication.loggedUser.id).child("profile").setValue(MainApplication.loggedUser.tojsonparams());
                        CommonWidget.showOKDialog(BuildProfileActivity.this, "", "Welcome "+MainApplication.loggedUser.firstName+" we hope you will enjoy playing with more firends as "+MainApplication.loggedUser.ingameName, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                startMenuActivity();
                            }
                        });                    }
                    if(responseObject.getString("status").equals("0")){
                        CommonWidget.showAlertDialog(BuildProfileActivity.this, "", getResources().getString(R.string.msgprofileerror), false);
                    }
                } catch (JSONException e) {
                    Log.e("Responsedata",e.getMessage());
                } catch (Exception e) {
                    Log.e("Responsedata", e.getMessage());
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                CommonWidget.showAlertDialog(BuildProfileActivity.this, "", getResources().getString(R.string.msgprofileerror), false);
                Log.e("Error.Response", error.toString());
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params =MainApplication.loggedUser.tojsonparams();
                return params;
            }

        };
        dialog = ProgressDialog.show(BuildProfileActivity.this, "",
                "Loading. Please wait...", true);

        queue.add(stringRequest);
    }
    public void startMenuActivity(){
        Intent intent=new Intent(this,MenuActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

}
