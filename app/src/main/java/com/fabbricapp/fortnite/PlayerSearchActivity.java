package com.fabbricapp.fortnite;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.fabbricapp.fortnite.model.Stat;
import com.fabbricapp.fortnite.utils.CommonWidget;
import com.fabbricapp.fortnite.webServices.WebserviceURLs;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class PlayerSearchActivity extends AppCompatActivity {
    private final String TAG="PlayerSearchActivity";
    EditText ingamenameet;
    ImageButton imgb1,imgb2,imgb3;
    Button btn;
    String platform;
    ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player_search);
        ingamenameet=findViewById(R.id.menubtn1);
        imgb1=findViewById(R.id.imageButton);
        imgb2=findViewById(R.id.imageButton2);
        imgb3=findViewById(R.id.imageButton3);
        btn=findViewById(R.id.button3);
        imgb1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                platform="psn";
            }
        });
        imgb2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                platform="pc";
            }
        });
        imgb3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                platform="xbl";
            }
        });
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(platform!=null&&!ingamenameet.getText().toString().equals("")){
                    synch(ingamenameet.getText().toString(),platform, CommonWidget.getApi());
                }
            }
        });

    }

    public void synch(String name,String platform, final String api){
        final String url = "http://api.fortnitetracker.com/v1/profile/"+platform+"/"+name+"?TRN-Api-Key="+api;
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    //Creating JsonObject from response String
                    Log.e(TAG,response);
                    JSONObject responseObject= new JSONObject(response.toString());
                    Log.e(TAG,responseObject+"");
                    if(responseObject.has("error")){
                        CommonWidget.showAlertDialog(PlayerSearchActivity.this,"",getResources().getString(R.string.oops),false);
                        dialog.dismiss();
                    }
                    else if(responseObject.has("stats")){
                        dialog.dismiss();
                        JSONObject stat=responseObject.getJSONObject("stats");
                        Stat playerstats=new Stat(stat);
                        Log.e(TAG,stat.toString());
                    }
                    else {
                        CommonWidget.showAlertDialog(PlayerSearchActivity.this,"",getResources().getString(R.string.oops),false);
                        dialog.dismiss();
                    }

                } catch (JSONException e) {
                    Log.e("Responsedata",e.getMessage());
                    CommonWidget.showAlertDialog(PlayerSearchActivity.this,"",getResources().getString(R.string.oops),false);
                    dialog.dismiss();
                } catch (Exception e) {
                    Log.e("Responsedata", e.getMessage());
                    CommonWidget.showAlertDialog(PlayerSearchActivity.this,"",getResources().getString(R.string.oops),false);
                    dialog.dismiss();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                CommonWidget.showAlertDialog(PlayerSearchActivity.this,"",getResources().getString(R.string.oops),false);
                dialog.dismiss();
                Log.e("Error.Response", error.toString());
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params =new HashMap<String, String>();
                params.put("TRN-Api-Key",api);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params =new HashMap<String, String>();
                params.put("TRN-Api-Key",api);
                return params;
            }
        };
        try {
            Log.e(TAG,""+stringRequest.getHeaders()+toString());
        } catch (AuthFailureError authFailureError) {
            authFailureError.printStackTrace();
        }
        dialog = ProgressDialog.show(PlayerSearchActivity.this, "",
                "Loading. Please wait...", true);

        queue.add(stringRequest);
    }


}
