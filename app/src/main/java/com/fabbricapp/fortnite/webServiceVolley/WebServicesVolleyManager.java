package com.fabbricapp.fortnite.webServiceVolley;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.JsonArray;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Ghassen on 10/07/2018.
 */

public class WebServicesVolleyManager {
    private static RequestQueue queue;
    private static JSONObject responseObject;

    public static void GetUser(Context context, String name, String lastname){
        final String url = "http://192.168.1.5/fortnite/test.php";
        queue = Volley.newRequestQueue(context);
        // prepare the Request
        StringRequest getRequest = new StringRequest (Request.Method.POST, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // display response
                        try {
                            responseObject= new JSONObject(response.toString());
                            Log.e("Responsedata",responseObject.toString());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("Error.Response", error.getMessage());
                    }
                }
        ){
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("nom", lastname);
                params.put("prenom", name);

                return params;
            }
        };

        // add it to the RequestQueue
        queue.add(getRequest);
    }

}
