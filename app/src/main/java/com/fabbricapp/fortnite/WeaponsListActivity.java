package com.fabbricapp.fortnite;

import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.fabbricapp.fortnite.adapter.RecyclerViewAdapter;
import com.fabbricapp.fortnite.model.Weapon;

import java.util.ArrayList;
import java.util.Date;

public class WeaponsListActivity extends AppCompatActivity {
    Cursor c = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weapons_list);
        ArrayList<Weapon> weapons=Weapon.getall();
        for (Weapon item:
             weapons) {
            Log.e("sss",item+"");
        }

        RecyclerView recyclerView = findViewById(R.id.recyclerv_view);
        RecyclerViewAdapter adapter = new RecyclerViewAdapter(this, weapons);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));


    }
}
