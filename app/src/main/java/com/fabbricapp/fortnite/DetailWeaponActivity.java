package com.fabbricapp.fortnite;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.fabbricapp.fortnite.model.Weapon;

public class DetailWeaponActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_weapon);

        Weapon weapon = (Weapon) getIntent().getSerializableExtra("weapon");
        Log.e("WeaponDeatail ", weapon.toString());
    }
}
