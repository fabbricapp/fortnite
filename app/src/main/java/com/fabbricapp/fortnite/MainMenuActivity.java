package com.fabbricapp.fortnite;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainMenuActivity extends AppCompatActivity implements View.OnClickListener {
    Button b1,b2,b3,b4,b5;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);
        b1=findViewById(R.id.menubtn1);
        b2=findViewById(R.id.menubtn2);
        b3=findViewById(R.id.menubtn3);
        b4=findViewById(R.id.menubtn4);
        b5=findViewById(R.id.menubtn5);
        b1.setOnClickListener(this);
        b2.setOnClickListener(this);
        b3.setOnClickListener(this);
        b4.setOnClickListener(this);
        b5.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.menubtn1:
                startActivity(new Intent(MainMenuActivity.this,PlayerSearchActivity.class));
                break;
        }
    }
}
