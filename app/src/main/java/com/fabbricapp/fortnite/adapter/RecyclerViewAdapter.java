package com.fabbricapp.fortnite.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.fabbricapp.fortnite.DetailWeaponActivity;
import com.fabbricapp.fortnite.R;
import com.fabbricapp.fortnite.model.Weapon;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Ghassen on 06/07/2018.
 */

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

    private ArrayList<Weapon> mWeeapons = new ArrayList<>();
    private Context mContext;
    public RecyclerViewAdapter(Context context, ArrayList<Weapon> Weeapons ) {
        mWeeapons = Weeapons;
        mContext = context;
    }

    @Override
    public RecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.weapon_item, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerViewAdapter.ViewHolder holder, int position) {
        Picasso.get()
                .load(mWeeapons.get(position).img)
                .into(holder.image);
        holder.name.setText(mWeeapons.get(position).name);
        holder.dps.setText(mWeeapons.get(position).dps);
        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, DetailWeaponActivity.class);
                intent.putExtra("weapon", mWeeapons.get(position));
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mWeeapons.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView image;
        TextView name;
        TextView dps;
        View view;
        public ViewHolder(View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.ivWeapon);
            name = itemView.findViewById(R.id.txtWeaponName);
            dps = itemView.findViewById(R.id.txtWeaponDps);
            view=itemView;
        }
    }
}
