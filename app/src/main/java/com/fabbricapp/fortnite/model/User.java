package com.fabbricapp.fortnite.model;

import android.util.ArrayMap;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by marwensarraj on 04/07/2018.
 */

public class User {
    public int id=0;
    public String facebookId;
    public String firstName;
    public String lastName;
    public String profileImg;
    public String deviceToken;
    public String ingameName;
    public String avatar;
    public String serverName;
    public String platformName;
    public String party;
    public String stat;




    public Map<String,String> tojsonparams() {
        Map<String,String> res=new HashMap<>();
        if(facebookId!=null)
        res.put("facebookId",facebookId);
        if(firstName!=null)
        res.put("firstName",firstName);
        if(lastName!=null)
        res.put("lastName",lastName);
        if(profileImg!=null)
        res.put("profileImg",profileImg);
        if(deviceToken!=null)
        res.put("deviceToken",deviceToken);
        if(ingameName!=null)
        res.put("ingameName",ingameName);
        if(avatar!=null)
        res.put("avatar",avatar);
        if(serverName!=null)
        res.put("serverName",serverName);
        if(platformName!=null)
        res.put("platformName",platformName);
        if(party!=null)
        res.put("party",party);
        if(stat!=null)
        res.put("stat",stat);
        if(id!=0)
            res.put("id",id+"");

        return res;
    }
    public void setData(JSONObject result) throws Exception {

        if (result.has("facebookId")) {
            this.facebookId = result.getString("facebookId");
        }

        if (result.has("id")) {
            this.id = result.getInt("id");
        }

        if (result.has("firstName")) {
            this.firstName = result.getString("firstName");
        }

        if (result.has("lastName")) {
            this.lastName = result.getString("lastName");
        }
        if (result.has("avatar")) {
            this.avatar = result.getString("avatar");
        }
        if (result.has("serverName")) {
            this.serverName = result.getString("serverName");
        }
        if (result.has("platformName")) {
            this.platformName = result.getString("platformName");
        }
        if (result.has("party")) {
            this.party = result.getString("party");
        }
        if (result.has("stat")) {
            this.stat = result.getString("stat");
        }
    }
    @Override
    public String toString() {
        return "id: " + id +
                "\n first name: " + firstName +
                "\n lastName: " + lastName +
                "\n profileImg: " + profileImg;
    }
}
