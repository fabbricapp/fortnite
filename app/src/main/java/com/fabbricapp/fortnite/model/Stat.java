package com.fabbricapp.fortnite.model;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by marwensarraj on 13/07/2018.
 */

public class Stat {
    public String solo_win;
    public String solo_kills;
    public String solo_winratio;
    public String solo_matches;
    public String duo_win;
    public String duo_kills;
    public String duo_winratio;
    public String duo_matches;
    public String squad_win;
    public String squad_kills;
    public String squad_winratio;
    public String squad_matches;

    public Stat(JSONObject json) {
        try {
            this.solo_win=json.getJSONObject("p2").getJSONObject("top1").getInt("valueInt")+"";
            this.solo_kills=json.getJSONObject("p2").getJSONObject("kills").getInt("valueInt")+"";
            this.solo_winratio=json.getJSONObject("p2").getJSONObject("winRatio").getString("displayValue");
            this.solo_matches=json.getJSONObject("p2").getJSONObject("matches").getInt("valueInt")+"";
            this.duo_win=json.getJSONObject("p10").getJSONObject("top1").getInt("valueInt")+"";
            this.duo_kills=json.getJSONObject("p10").getJSONObject("kills").getInt("valueInt")+"";
            this.duo_winratio=json.getJSONObject("p10").getJSONObject("winRatio").getString("displayValue");
            this.duo_matches=json.getJSONObject("p10").getJSONObject("matches").getInt("valueInt")+"";
            this.squad_win=json.getJSONObject("p9").getJSONObject("top1").getInt("valueInt")+"";
            this.squad_kills=json.getJSONObject("p9").getJSONObject("kills").getInt("valueInt")+"";
            this.squad_winratio=json.getJSONObject("p9").getJSONObject("winRatio").getString("displayValue");
            this.squad_matches=json.getJSONObject("p9").getJSONObject("matches").getInt("valueInt")+"";

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String toString() {
        return "public String"+ solo_win+
        "public String"+ solo_kills+
        "public String"+ solo_winratio+
        "public String"+ solo_matches+
        "public String"+ duo_win+
        "public String"+ duo_kills+
        "public String"+ duo_winratio+
        "public String"+ duo_matches+
        "public String"+ squad_win+
        "public String"+ squad_kills+
        "public String"+ squad_winratio+
        "public String"+ squad_matches;
    }
}
