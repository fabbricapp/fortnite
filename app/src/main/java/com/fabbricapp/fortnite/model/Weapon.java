package com.fabbricapp.fortnite.model;

import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;

import com.fabbricapp.fortnite.utils.DBHelper;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by marwensarraj on 06/07/2018.
 */

public class Weapon implements Serializable {
    public String name;
    public String rarity;
    public String dps;
    public String dmg;
    public String envDmg;
    public String fireRate;
    public String magazine;
    public String reloadTime;
    public String img;

    public Weapon() {
    }

    public Weapon(String name, String rarity, String dps, String dmg, String envDmg, String fireRate, String magazine, String reloadTime, String img) {
        this.name = name;
        this.rarity = rarity;
        this.dps = dps;
        this.dmg = dmg;
        this.envDmg = envDmg;
        this.fireRate = fireRate;
        this.magazine = magazine;
        this.reloadTime = reloadTime;
        this.img = img;
    }

    public static ArrayList<Weapon> getall(){
        ArrayList<Weapon>result=new ArrayList<>();
        //TODO: get all from sqlite
        Cursor cursor=DBHelper.query("Weapons");
        while(cursor.moveToNext()){
            Weapon weapon=new Weapon();
            weapon.name=cursor.getString(0);
            weapon.rarity=cursor.getString(1);
            weapon.dps=cursor.getString(2);
            weapon.dmg=cursor.getString(3);
            weapon.envDmg=cursor.getString(4);
            weapon.fireRate=cursor.getString(5);
            weapon.magazine=cursor.getString(6);
            weapon.reloadTime=cursor.getString(7);
            weapon.img=cursor.getString(8);
            result.add(weapon);
        }
        return result;
    }
    public Weapon(Parcel in){
        name=in.readString();
        rarity=in.readString();
        dps=in.readString();
        dmg=in.readString();
        envDmg=in.readString();
        fireRate=in.readString();
        magazine=in.readString();
        reloadTime=in.readString();
        img=in.readString();

    }
    public static final Parcelable.Creator<Weapon> CREATOR = new Parcelable.Creator<Weapon>() {

        @Override
        public Weapon createFromParcel(Parcel parcel) {
            return new Weapon(parcel);
        }

        @Override
        public Weapon[] newArray(int i) {
            return new Weapon[0];
        }
    };
    @Override
    public String toString() {
        return "name: " + name +
                "\n dps: " + dps +
                "\n img: " + img ;
    }

}
