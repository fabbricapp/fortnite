package com.fabbricapp.fortnite;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import com.fabbricapp.fortnite.utils.PinchZoomImageView;

public class MapActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PinchZoomImageView img = new PinchZoomImageView(this);
        img.setImageResource(R.drawable.fortnitemap);
        img.setMaxZoom(4f);
        setContentView(img);
    }

}


